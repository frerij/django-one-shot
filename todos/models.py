from unittest.util import _MAX_LENGTH
from django.db import models
from django.conf import settings

# Create two models 

# TodoList Model
# name < 100 characters
# created_on containing date and time of creation by default
#   use auto_now_add feature
# __str__ method returning value of name property
#TEST: python manage.py test tests.todo_lists.test_model

class TodoList(models.Model):
    name = models.CharField(max_length = 100)
    created_on = models.DateTimeField(auto_now_add = True)
    def __str__(self):
        return self.name


# TodoItem Model
# task < 100 characters
# due_date (optional) containing date and time for completion
# is_completed - boolean True if completed, False if not
# list - ForeignKey to TodoList that creates a related collection ("items")
#   and will delete items if list is deleted
# __str__ method returning value of task property
# TEST: python manage.py test tests.todo_items.test_model

class TodoItem(models.Model):
    task = models.CharField(max_length = 100)
    description = models.CharField(max_length = 250)
    due_date = models.DateTimeField(auto_now_add = False, null = True, blank = True)
    is_completed = models.BooleanField(default = False, null = False)
    # best way to name?
    #list = models.ForeignKey(TodoList, related_name = 'items', on_delete = models.CASCADE)
    def  __str__(self):
        return self.task