from django.contrib import admin
from todos.models import (
    TodoItem,
    TodoList
)

# registering admin models
class TodoItemAdmin(admin.ModelAdmin):
    pass

class TodoListAdmin(admin.ModelAdmin):
    pass

admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)