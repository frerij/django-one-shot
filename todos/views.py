from dataclasses import fields
from django.urls import reverse_lazy
from django.views.generic import DetailView, CreateView, DeleteView, UpdateView, ListView

from todos.models import TodoItem, TodoList

# TodoList views:
#   list, detail, create, update, delete

class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    
     

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("all_todos_lists")

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"


# TodoItem views:
#   create, update

class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    fields = ["task", "description", "due_date", "is_completed"]

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    fields = ["task", "description", "due_date", "is_completed"]