from django.contrib import admin
from django.urls import include, path

from todos.views import (
    TodoItemCreateView,
    TodoItemUpdateView,
    TodoListCreateView,
    TodoListDeleteView,
    TodoListListView,
    TodoListDetailView,
    TodoListUpdateView,

)

urlpatterns = [
    path("", TodoListListView.as_view(), name = "all_todos_lists"),
    path("todos/<int:pk>/", TodoListDetailView.as_view(), name = "todos_detail"),
    path("todos/create/", TodoListCreateView.as_view(), name = "todo_list_create"),
    path("todos/<int:pk>/edit/", TodoListUpdateView.as_view(), name = "todo_list_edit"),
    path("todos/<int:pk>/delete/", TodoListDeleteView.as_view(), name = "todo_list_delete"),
    path("todos/items/create", TodoItemCreateView.as_view(), name = "todo_item_create"),
    path("todos/items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name = "todo_item_edit"),

    
]